﻿namespace NameSwitcher
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.filePath = new System.Windows.Forms.TextBox();
            this.ActionButton = new System.Windows.Forms.Button();
            this.sourceText = new System.Windows.Forms.TextBox();
            this.switchText = new System.Windows.Forms.TextBox();
            this.isFullSwitch = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // filePath
            // 
            this.filePath.Location = new System.Drawing.Point(223, 59);
            this.filePath.Name = "filePath";
            this.filePath.Size = new System.Drawing.Size(565, 20);
            this.filePath.TabIndex = 0;
            // 
            // ActionButton
            // 
            this.ActionButton.Location = new System.Drawing.Point(357, 330);
            this.ActionButton.Name = "ActionButton";
            this.ActionButton.Size = new System.Drawing.Size(154, 65);
            this.ActionButton.TabIndex = 1;
            this.ActionButton.Text = "Заменить";
            this.ActionButton.UseVisualStyleBackColor = true;
            this.ActionButton.Click += new System.EventHandler(this.ActionButton_Click);
            // 
            // sourceText
            // 
            this.sourceText.Location = new System.Drawing.Point(89, 195);
            this.sourceText.Name = "sourceText";
            this.sourceText.Size = new System.Drawing.Size(237, 20);
            this.sourceText.TabIndex = 2;
            // 
            // switchText
            // 
            this.switchText.Location = new System.Drawing.Point(551, 195);
            this.switchText.Name = "switchText";
            this.switchText.Size = new System.Drawing.Size(237, 20);
            this.switchText.TabIndex = 3;
            // 
            // isFullSwitch
            // 
            this.isFullSwitch.Checked = true;
            this.isFullSwitch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.isFullSwitch.Location = new System.Drawing.Point(333, 234);
            this.isFullSwitch.Name = "isFullSwitch";
            this.isFullSwitch.Size = new System.Drawing.Size(194, 63);
            this.isFullSwitch.TabIndex = 4;
            this.isFullSwitch.Text = "Заменить только часть имени";
            this.isFullSwitch.UseMnemonic = false;
            this.isFullSwitch.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Исходный текст";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(616, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(622, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Заменяемый текст";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Путь к папке с файлами";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(398, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "заменить на";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.isFullSwitch);
            this.Controls.Add(this.switchText);
            this.Controls.Add(this.sourceText);
            this.Controls.Add(this.ActionButton);
            this.Controls.Add(this.filePath);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox filePath;
        private System.Windows.Forms.Button ActionButton;
        private System.Windows.Forms.TextBox sourceText;
        private System.Windows.Forms.TextBox switchText;
        private System.Windows.Forms.CheckBox isFullSwitch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

