﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
namespace NameSwitcher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
 

        private void ActionButton_Click(object sender, EventArgs e)
        {
           
            try { Switcher(); }

            catch (Exception errorSwitch)
            {
                MessageBox.Show(errorSwitch.Message);
            }

        }

        void Switcher()
        {
            string countSTR = string.Empty;
            string newFileName;

            int count = 0;

            bool isNotFind = true;

            FileInfo[] files = new DirectoryInfo(@filePath.Text).GetFiles();

            
            foreach (var file in files)
            {
                if (Regex.IsMatch(file.Name, "\\b" + sourceText.Text + "\\b"))
                {
                    isNotFind = false;
                    count++;

                    if (isFullSwitch.Checked) newFileName = Regex.Replace(file.Name, "\\b" + sourceText.Text + "\\b", switchText.Text);

                    else
                    {
                        newFileName = String.Format("{0} {1}{2}" ,switchText.Text, countSTR, file.Extension);
                         

                        countSTR = count.ToString();
                    }

                  

                    string fullFileName = Path.Combine(file.DirectoryName, newFileName);

                    File.Move(file.FullName, fullFileName);
                }
            }


            if (isNotFind) throw new Exception("Совпадений в имени файлов не найдено, переименование невозможно");
            else MessageBox.Show(String.Format("Переименование выполненено успешно, переименовано {0} файлов", count));

        }

        
    }
}
